const express = require('express');
const socket = require('socket.io');

const app = express();

const server = app.listen(4000, () => {
  console.log('Listening on port 4000');
});

app.use(express.static('public'));

const io = socket(server);

io.on('connection', (socket) => {
  console.log('User connected: ' + socket.id);
  socket.emit('connected', socket.id);

  socket.on('join', (roomName) => {
    const rooms = io.sockets.adapter.rooms;
    const room = rooms.get(roomName);

    if (!room) {
      socket.join(roomName);
      socket.emit('created');
    } else if (room.size < 5) {
      socket.join(roomName);
      socket.emit('joined');
    } else {
      socket.emit('full');
    }
  });

  socket.on('ready', (roomName, fromId) => {
    console.log('Ready', fromId);
    socket.broadcast.to(roomName).emit('ready', fromId);
  });

  socket.on('candidate', (candidate, roomName, fromId, toId) => {
    console.log('Candidate', candidate);
    socket.broadcast.to(roomName).emit('candidate', candidate, fromId, toId);
  });

  socket.on('offer', (offer, roomName, fromId, toId) => {
    console.log('Offer', offer);
    socket.broadcast.to(roomName).emit('offer', offer, fromId, toId);
  });

  socket.on('answer', (answer, roomName, fromId, toId) => {
    console.log('Answer', answer);
    socket.broadcast.to(roomName).emit('answer', answer, fromId, toId);
  });

  socket.on('leave', (roomName, fromId) => {
    console.log('Leave', roomName, fromId);
    socket.leave(roomName);
    socket.broadcast.to(roomName).emit('leave', fromId);
  });
});
